import React, { Component } from 'react';
import ListDevices from './components/ListDevices';
import Log from './components/Log';
import Form from './components/Form';
import './App.css';

class App extends Component {
  state = {
    groups: [],
    devices: [],
    logs: []
  }

  componentDidMount = () => {
    this.refreshData();
  }

  refreshData = () => {
    fetch('/api/device')
    .then(res => res.json())
    .then((res) => this.setState({ devices: res }));
    
  }

  refreshLog = (id) => {
     fetch(`/api/log/${id}`, {
      method: 'GET',
      headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
      }
  })
  .then(res => res.json())
  .then((res) => this.setState({ logs: res }));
  }

  render() {
    return (
      <div className="App">
        <ListDevices devices={this.state.devices} onChange={this.refreshData} onLog={this.refreshLog} />
        <Form onAdd = {this.refreshData}/>
        <Log logs = {this.state.logs}/>
      </div>
    );
  }
}

export default App;
