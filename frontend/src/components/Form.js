import React, { Component } from 'react';

class Form extends Component {

    constructor(props) {
        super(props);
    
        this.handleSubmit = this.handleSubmit.bind(this);
      }
     

      handleSubmit(event) {
          event.preventDefault();
       console.log(event.target)
       const data = { 
           name: event.target["deviceName"].value,
           address: event.target["deviceAddress"].value
        };

       fetch('api/device', {
            method: 'POST',
            headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
            },
            
            body: JSON.stringify({device: data})
        })
        .then(function(res){ console.log(res) })
        .catch(function(res){ console.log(res) })

        this.props.onAdd();
    }

    render() {
        return (
            <form className="form-inline" onSubmit={this.handleSubmit}>
                <div class="form-group mb-2">
                    <label for="deviceName" className="sr-only">Device Name</label>
                    <input 
                    type="text"
                    className="form-control"
                    id="deviceName"
                    name="deviceName"
                    placeholder="Device Name" />
                </div>
                <div class="form-group mx-sm-3 mb-2">
                    <label for="deviceAddress" class="sr-only">IP Address</label>
                    <input 
                    type="text" 
                    className="form-control" 
                    id="deviceAddress" 
                    name ="deviceAddress" 
                    placeholder="IP Address" />
                </div>
                <button type="submit" class="btn btn-primary mb-2">Add Device</button>
            </form>
        );
    }
}

export default Form;
