import React, { Component } from 'react';

class Log extends Component {

  

    renderLog(index) {
        const log = this.props.logs[index];

        return (
            <tr>
                <th scope="row">{index + 1}</th>
                <td>{log.device.name}</td>
                <td>{log.state === true ? "On" : "Off"}</td>
                <td>{log.date}</td>
            </tr>
        );
    }

    render() {
        const logs = this.props.logs.map((log, index) => this.renderLog(index));

        return (
            <table className="table">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Device Name</th>
                        <th scope="col">State</th>
                        <th scope="col">Date</th>
                    </tr>
                </thead>
                <tbody>
                    { logs }
                </tbody>
            </table>
        );
    }
}

export default Log;