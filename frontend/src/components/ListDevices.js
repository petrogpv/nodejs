import React, { Component } from 'react';

class ListDevices extends Component {

    deleteHandler = async (device) => {
        await fetch(`/api/device/${device.id}`, {
            method: 'DELETE',
            headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
            }
        })
        .then(function(res){ console.log(res) })
        .catch(function(res){ console.log(res) })

        this.props.onChange();
    }

    logHandler =  (id) => {
          this.props.onLog(id);
    }
    
    onUpdateStatus = async (id, isOn) =>{
        await fetch('/api/device/' + id, {
            method: 'PUT',
            headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
            },
            body: JSON.stringify({isOn})
        })
        this.props.onChange();
    }

    renderDevice(index) {
        const device = this.props.devices[index];
        let onBtn, offBtn;

        if (device.isOn) {
            onBtn = 'btn btn-primary';
            offBtn = 'btn btn-outline-secondary';
        } else {
            offBtn = 'btn btn-primary';
            onBtn = 'btn btn-outline-secondary';
        }

        return (
            <tr>
                <th scope="row">{index + 1}</th>
                <td>{device.name}</td>
                <td>{device.address}</td>
                <td>{device.groups}</td>
                <td>
                    <div className="btn-toolbar float-right" role="toolbar">
                        <div className="btn-group mr-2" role="group">
                            <button type="button" className={onBtn} onClick = {this.onUpdateStatus.bind(this, device.id, true)} >On</button>
                            <button type="button" className={offBtn}  onClick = {this.onUpdateStatus.bind(this, device.id, false)} >Off</button>
                        </div>

                        <div className="btn-group mr-2" role="group">
                            <button
                             type="button"
                             className="btn btn-outline-warning"
                             onClick = {this.logHandler.bind(this, device.id)} >Logs</button>
                        </div>

                        <div className="btn-group" role="group">
                            <button
                             type="button"
                             className="btn btn-outline-danger"
                            onClick = {this.deleteHandler.bind(this, device)}>Delete</button>
                        </div>
                    </div>
                </td>
            </tr>
        );
    }

    render() {
        const devices = this.props.devices.map((device, index) => this.renderDevice(index));

        return (
            <table className="table">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Device Name</th>
                        <th scope="col">Device Address</th>
                        <th scope="col">Device Groups</th>
                        <th scope="col">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    { devices }
                </tbody>
            </table>
        );
    }
}

export default ListDevices;