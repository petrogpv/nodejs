const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const deviceRouter = require('./controllers/device');
const logRouter = require('./controllers/log');
const groupRouter = require('./controllers/group');
const mongoose  = require('mongoose');

mongoose.connect ('mongodb://localhost/node-workshop');

app.use(express.json());
app.use('/api/device', deviceRouter);
app.use('/api/log', logRouter);
app.use('/api/group', groupRouter);

app.get('/', (req, res) => {
    res.json({
        status: "Ok"
    });
}); 

app.listen(3001);
