const mongoose = require('mongoose');
const Log = require("../models/log");
const Group = require('../models/group');


const deviceSchema = new mongoose.Schema({
    name: String,
    address: String,
    isOn: Boolean,
    groups: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Group' }]
  });

  deviceSchema.pre('remove', function(next) {
    Log.remove({device: this}).exec();
    next();
});

const Device = mongoose.model('Device', deviceSchema);

module.exports = Device; 