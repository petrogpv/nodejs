const mongoose = require('mongoose');
const Device = require('../models/device');

const Group = mongoose.model('Group', {
    name: String,
    isOn: Boolean,
    devices: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Device' }]
});

module.exports = Group; 