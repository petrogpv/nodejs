const mongoose = require('mongoose');
const Device = require('../models/device');

const Log = mongoose.model('Log', {
    device: { type: mongoose.Schema.Types.ObjectId, ref: 'Device' },
    state: Boolean,
    date: { type: Date, default: Date.now }
});

module.exports = Log; 