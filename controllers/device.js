const router = require('express').Router();
const Device = require("../models/device");
const Log = require("../models/log");
const Gorup = require("../models/group");
const fetchUrl = require("fetch").fetchUrl;


router.get('/', (req, res) => {
    Device.find((err, docs)=>{
        if(err){
            res.sendStatus(500);
            return;
        }
        const devices = docs.map(doc => ({
            id: doc._id,
            name: doc.name,
            address: doc.address,
            isOn: doc.isOn
        }));
        res.json(devices);
    })
});

router.post('/', async (req, res) =>{
    const device = req.body.device;
    await Device.create({
        name: device.name,
        address: device.address,
        isOn: false
    });

    res.sendStatus(201);
});

router.delete('/:id', async (req, res) =>{
    const id = req.params.id;

        await Device.findById(id, function(err, device) {

            if(err){
                res.sendStatus(500);
                return;
            }
        
            device.remove();

            res.sendStatus(200);
        });      
});

router.put('/:id', async (req, res) =>{
    const isOn = req.body.isOn;
    const id = req.params.id;

    const device = await Device.findById(id);
    const command = '/cm?cmnd=' + (isOn ? 'on' : 'off');

    fetchUrl(device.address + command, async function(error, meta, body) {
        device.isOn = isOn;
        await device.save();
        await Log.create({
            device: id,
            address: device.address,
            state: isOn
        });

        res.sendStatus(200);
    });
    // await Device.findByIdAndUpdate(id, {
    //     isOn
    // }).exec();

    // res.sendStatus(200);
});

module.exports = router;