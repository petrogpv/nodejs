const router = require('express').Router();
const Log = require("../models/log");
const Device = require("../models/device");
const fetchUrl = require("fetch").fetchUrl;


router.get('/:id', (req, res) => {
    // Log.remove({}, (err) => {
    //     if(err){
    //         res.sendStatus(500);
    //         return;
    //     }
    // });

    const deviceId = req.params.id;
    console.log(deviceId);
    Log.find().populate({
        path: 'device',
        // match:{id :deviceId}
    })
    .exec((err, docs)=>{
        if(err){
            res.sendStatus(500);
            return;
        }
        
        const logs = docs.map(doc => ({
            id: doc._id,
            device: doc.device,
            state: doc.state,
            date: doc.date
        })).filter((log) => log.device.id === deviceId);
        res.json(logs);
    });
   
});

module.exports = router;