const express = require('express');
const app = express();

const port = parseInt(process.argv[2]);

app.get('/cm', (req, res) => {
    const cmnd = req.query.cmnd;
    if(cmnd === 'on'){
        console.log('On');
    }

    if(cmnd === 'off'){
        console.log('Off');
    }
    res.sendStatus(200);
});

app.listen(port, function(err){
    if(!err){
        console.log(`Fake device is on 127.0.0.1:${port}`);
    }
})